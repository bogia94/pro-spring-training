package com.prospring.ch02;

public interface MessageProvider {
    String getMessage();
}
